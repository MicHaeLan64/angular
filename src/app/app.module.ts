import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './component/common/navbar/navbar.component';
import { UserComponent } from './component/user/user.component';
import { LoginComponent } from './component/login/login.component';

import { AuthGuard } from './auth.guard';
import { BranchService } from './service/branch/branch.service';
import { LoginService } from './service/login/login.service';
import { UserService } from './service/user/user.service';
import { AuthService } from './service/auth/auth.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UserComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    AuthGuard,
    BranchService,
    LoginService,
    UserService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
