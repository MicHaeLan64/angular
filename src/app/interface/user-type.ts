export interface UserType {
  id: number;
  name: string;
  permission: string;
  locale: string;
}
