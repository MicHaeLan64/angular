export interface ResponseType {
  code: number;
  data: any;
}
