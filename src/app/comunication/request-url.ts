export class RequestUrl {
  public static BRANCH_URL = 'http://127.0.0.1:8000/branch/all';
  public static LOGIN_URL = 'http://127.0.0.1:8000/user/login';
  public static USER_URL = 'http://127.0.0.1:8000/user/';
}
