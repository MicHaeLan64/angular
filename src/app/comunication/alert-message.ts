export class AlertMessage {
  public static NETWORK_ERROR = 'Error - Please check your network.';
  public static LOGIN_ERROR = 'Error - You have entered an invalid username password combination. Please try again.';
}
