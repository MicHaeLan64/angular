import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BranchService } from '../../service/branch/branch.service';
import { LoginService } from '../../service/login/login.service';

import { AlertMessage } from '../../comunication/alert-message';
import { BranchType } from '../../interface/branch-type';
import { ResponseCode } from '../../comunication/response-code';
import { AuthService } from '../../service/auth/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  networkError: Boolean;
  loginError: Boolean;
  uname: string;
  pwd: string;
  branch: number;
  msg: string;
  branches: BranchType[];

  constructor(
    private router: Router,
    private branchService: BranchService,
    private loginService: LoginService,
    private authService: AuthService
  ) {
    this.networkError = false;
    this.loginError = false;
    this.branches = [];
  }

  ngOnInit() {
    this.getBranches();
  }

  login(): void {
    this.loginService.login(this.uname, this.pwd, this.branch).subscribe(res => {
      if (res['code'] === ResponseCode.CODE_OK) {
        this.authService.setLogin(res['data']);
        this.router.navigate(['/user']);
      } else {
        this.loginError = true;
        this.msg = AlertMessage.LOGIN_ERROR;
      }
    });
  }

  getBranches(): void {
    this.branchService.getBranches().subscribe(res => {
      if (res['code'] === ResponseCode.CODE_OK) {
        this.networkError = false;
        for (const branchInfo of res['data']) {
          const data: BranchType = {'id': branchInfo.id, 'name': branchInfo.name};
          this.branches.push(data);
        }
      } else {
        this.networkError = true;
        this.msg = AlertMessage.NETWORK_ERROR;
      }
    });
  }
}
