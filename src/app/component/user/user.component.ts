import { Component, OnInit } from '@angular/core';

import { UserService } from '../../service/user/user.service';

import { UserType } from '../../interface/user-type';
import {ResponseCode} from '../../comunication/response-code';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users: UserType[];

  constructor(private userService: UserService) {
    this.users = [];
  }

  ngOnInit() {
    this.userService.all().subscribe(res => {
      if (res['code'] === ResponseCode.CODE_OK) {
        for (const data of res['data']) {
          const user: UserType = {'id': data.id, 'name': data.name, 'permission': data.permission, 'locale': data.locale};
          this.users.push(user);
        }
      }
    });
  }

}
