import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { RequestUrl } from '../../comunication/request-url';
import { ResponseType } from '../../interface/response-type';


@Injectable()
export class LoginService {

  constructor(private http: HttpClient) { }

  login(uname, pwd, branch): Observable<ResponseType> {
    return this.http.post<ResponseType>(RequestUrl.LOGIN_URL, {name: uname, pwd: pwd, branch: branch});
  }
}
