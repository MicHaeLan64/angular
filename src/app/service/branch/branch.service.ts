import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { RequestUrl } from '../../comunication/request-url';
import { ResponseType } from '../../interface/response-type';

@Injectable()
export class BranchService {

  constructor(private http: HttpClient) { }

  getBranches(): Observable<ResponseType> {
    return this.http.get<ResponseType>(RequestUrl.BRANCH_URL);
  }
}
