import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { RequestUrl } from '../../comunication/request-url';
import { ResponseType } from '../../interface/response-type';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  all(): Observable<ResponseType> {
    return this.http.get<ResponseType>(RequestUrl.USER_URL);
  }
}
