import { Injectable } from '@angular/core';
import { UserType } from '../../interface/user-type';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthService {

  loggedIn: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(this.hasUser());

  constructor() { }

  setLogin(user): void {
    localStorage.setItem('user', JSON.stringify(user));
    this.loggedIn.next(true);
  }

  setLogout(): void {
    localStorage.removeItem('user');
    this.loggedIn.next(false);
  }

  isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  getLoginUser(): Object {
    return JSON.parse(localStorage.getItem('user'));
  }

  hasUser(): Boolean {
    return localStorage.getItem('user') !== null;
  }
}
